<?php

namespace html_table_builder\html;

class Tag
{
    public static function getHeader($data, $options = null)
    {
        $html = '';
        foreach ($data as $item) {
            $html .= "<th". static::getOptions($options). ">" . $item . "</th>";
        }

        return static::getRow($html);
    }

    public static function getRow($data, $options = null)
    {
        return "<tr". static::getOptions($options). ">" . $data . "</tr>";
    }

    public static function getOptions($options)
    {

    }
}