<?php

namespace html_table_builder\table;

use html_table_builder\html\Tag;
use html_table_builder\classes\DataProvider;

class Table
{
    public $tableName;

    public $attributes = [];

    public $actionColumn = false;

    public $primaryKeyName = false;

    public $actionColumnTempalte = "{edit}\n{delete}\n{view}";

    protected $_defaultColumnTemplate = 'data';

    protected $_dataProvider;

    public function __construct($tableConfig)
    {
        $this->tableName = $tableConfig['table_name'];
        $this->attributes = $tableConfig['attribute'];

        if (isset($tableConfig['primary'])) {
            $this->primaryKey = $tableConfig['primary']['enable'] ?? false;
            $this->primaryKeyName = $tableConfig['primary']['attribute_name'] ?? $this->primaryKeyName;
        }

        if (isset($tableConfig['action'])) {
            $this->actionColumn = $tableConfig['action']['enable'] ?? false;;
            $this->actionColumnTempalte = $tableConfig['action']['template'] ?? $this->actionColumnTempalte;
        }

        $this->_init($tableConfig);
    }

    protected function _init($tableConfig)
    {
        $this->_getTableData();
        $this->_getTableHeader();
    }

    public function getTable()
    {
        return ;
    }

    protected function _getTableData()
    {
        $this->_dataProvider = new DataProvider($this->tableName);
    }

    protected function _getTableHeader()
    {
        $attribute = $this->_getAttributes();
        $this->header = Tag::getHeader($attribute);
    }

    protected function _getAttributes()
    {
        $attributes = [];
        foreach ($this->attributes as $attribute) {
            $name = is_array($attribute) ? ($attribute['name'] ?? false) : $attribute;
            if ($name) {
                $attributes[] = $name;
            }
        }

        return array_intersect($attributes, $this->_dataProvider->attribute);
    }
}