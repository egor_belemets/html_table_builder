<?php

namespace html_table_builder\classes;

//use \ReflectionAbstract;

class DataProvider extends ReflectionAbstract
{
    public $data;

    public $attribute;

    public function __construct($tableName)
    {
        $this->_init($tableName);
    }

    protected function _init($tableName)
    {
        $modelClass = 'Model\\' . ucfirst($tableName);

        try {
            $this->checkClassExist($modelClass);
        } catch (\ReflectionException $e) {

        }

        $this->attribute = array_keys((new $modelClass())->attributes());

        $this->data = $modelClass::find('all');
    }
}