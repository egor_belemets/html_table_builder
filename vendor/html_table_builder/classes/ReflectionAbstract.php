<?php

namespace html_table_builder\classes;

abstract class ReflectionAbstract
{
    protected function checkClassExist($className)
    {
        return (new \ReflectionClass($className))->inNamespace();
    }
}
