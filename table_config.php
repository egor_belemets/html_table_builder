<?php

return [
    'table_name' => 'customer',
    'attribute' => [
        [
            'name' => 'id',
            'template' => 'data',   // default data, can be set date, img, link
            'html_attribute' => [
                'class' => 'class_name',
                'id' => 'id_name',
                'data' => [
                    'name' => 'index',
                    'value' => 123
                ]
            ]
        ],
        'first_name',
        'email'
    ],
    'primary' => [
        'enable' => true,           // if it needs render primary key column - set true if not - false or without
        'attribute_name' => 'id'
    ],
    'action' => [
        'enable' => true,           // if it needs render action column - set true if not - false or without
        'template' => "{edit}\n{delete}\n{view}"
    ]
];
