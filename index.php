<?php

use html_table_builder\table\Table;

require_once 'vendor/autoload.php';

define('DS', DIRECTORY_SEPARATOR);

set_include_path(__DIR__.DIRECTORY_SEPARATOR.'Model');
set_include_path(__DIR__.DIRECTORY_SEPARATOR.'vendor');
spl_autoload_register( function($class) {
    $class = ltrim($class, '\\');
    $file = str_replace('\\', DIRECTORY_SEPARATOR, $class).'.php';
    require_once $file;
});

//require_once 'vendor/html_table_builder/autoload.php';

ActiveRecord\Config::initialize(function($cfg)
{
    $cfg->set_model_directory(__DIR__ . DS . 'Model');
    $cfg->set_connections(
        [
            'development' => 'mysql://username:password@localhost/db_name',
        ]
    );
});

$tableConfig = require_once 'table_config.php';

echo (new Table($tableConfig))->getTable();

